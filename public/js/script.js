$(document).ready(function(){
	$('.slider').slick({
		arrows:false,
		dots:false,
		slidesToShow:4,
		autoplay:true,
		speed:1500,
		adaptiveHeght:true,
		autoplaySpeed:4000,
		pauseOnHover:false,
		responsive:[
			{
				breakpoint: 768,
				settings: {
					slidesToShow:5
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow:1
				}
			}
		]
	});
	$('.slider_1').slick({
		arrows:false,
		dots:false,
		slidesToShow:5,
		autoplay:true,
		speed:1000,
		autoplaySpeed:4000,
		adaptiveHeght:true,
		pauseOnHover:false,
		responsive:[
			{
				breakpoint: 768,
				settings: {
					slidesToShow:2
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow:1
				}
			}
		]
	});
	$('.slider_2').slick({
		arrows:true,
		dots:false,
		slidesToShow:1,
		slidesToScrol:1,
		autoplay:false,
		speed:1000,
		adaptiveHeght:true,
		draggable: false,
		swipe:false, 
		centerMode: false,
		wvariableWidth:true,
		fade:true,
		responsive:[
			{
				breakpoint: 768,
				settings: {
					slidesToShow:2
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow:1
				}
			}
		]
	});

});
function done() {
document.getElementById('nick').value = "";
document.getElementById('email').value = "";
document.getElementById('message').value = "";
document.getElementById('tel').value= "";
$('#check').prop('checked', false);
localStorage.clear();
};

$(document).ready(function () {
$('#formData').on('submit', function (e) {
e.preventDefault();
$.ajax({
url: 'https://api.slapform.com/rukala@list.ru',
dataType: "json",
method: 'POST',
data: {
name: $('#nick').val(),
email: $('#email').val(),
tel:$('#tel').val(),
message: document.getElementById('message').value,
check: $('#check').is(':checked'),
slap_captcha: false
},
success: function (response) {
console.log('Got data: ', response);
if (response.meta.status == 'success') {
alert("Успешная отправка формы!)");
done();
} else if (response.meta.status == 'fail') {
alert("Ошибка!");
console.log('Submission failed with these errors: ', response.meta.errors);
}
}
});
});
});
